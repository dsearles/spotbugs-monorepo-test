# java-maven
Test project with:

* **Language:** Java
* **Package Manager:** Maven

## How to use

Please see the [usage documentation](https://gitlab.com/gitlab-org/security-products/tests/common#how-to-use-a-test-project) for Security Products test projects.

## Supported Security Products Features

| Feature             | Supported          |
|---------------------|--------------------|
| SAST                | :white_check_mark: |
| Dependency Scanning | :white_check_mark: |
| Container Scanning  | :x:                |
| DAST                | :x:                |
| License Management  | :white_check_mark: |

## Considerations for use of certificates

When using self-signed certificates or a custom certificate authority, you may need to configure
Maven with relaxed certificate checks when pulling from remote repositories. Please see the [Maven Wagon documentation for available options](https://maven.apache.org/wagon/wagon-providers/wagon-http/).

Additionally, please see the following Maven documentation:

* [Maven documentation on using internal repositories](https://maven.apache.org/guides/introduction/introduction-to-repositories.html#internal-repositories)
* [Maven documentation on using custom plugin repositories](http://maven.apache.org/guides/development/guide-testing-development-plugins.html)
